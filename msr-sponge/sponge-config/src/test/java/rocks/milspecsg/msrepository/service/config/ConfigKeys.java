package rocks.milspecsg.msrepository.service.config;

public abstract class ConfigKeys {
    public static final int PARTY_CREATE_COST_DOUBLE = 10;
    public static final int SOME_LIST = 15;
    public static final int SOME_MAP = 20;
    public static final int ANOTHER_MAP = 30;
}
