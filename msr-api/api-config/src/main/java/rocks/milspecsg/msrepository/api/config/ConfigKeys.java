package rocks.milspecsg.msrepository.api.config;

public abstract class ConfigKeys {

    public static final int CACHE_INVALIDATION_TIMOUT_SECONDS_INT = 0;
    public static final int CACHE_INVALIDATION_INTERVAL_SECONDS_INT = 1;

}
