package rocks.milspecsg.msrepository.model.config.map;

import java.util.Map;

public interface IntegerIntegerMap extends Map<Integer, Integer> {
}
