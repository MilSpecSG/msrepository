package rocks.milspecsg.msrepository.model.config.map;

import java.util.Map;

public interface StringStringMap extends Map<String, String> {
}
