package rocks.milspecsg.msrepository.model.config.map;

import java.util.Map;

public interface StringIntegerMap extends Map<String, Integer> {
}
