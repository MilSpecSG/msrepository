package rocks.milspecsg.msrepository.model.config.map;

import java.util.Map;

public interface IntegerStringMap extends Map<Integer, String> {
}
